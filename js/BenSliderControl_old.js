L.Control.SliderControl = L.Control.extend({
    options: {
        position: 'topright',
        layers: null,
        timeAttribute: 'time',
        isEpoch: false,     // whether the time attribute is seconds elapsed from epoch
        startTimeIdx: 0,    // where to start looking for a timestring
        timeStrLength: 19,  // the size of  yyyy-mm-dd hh:mm:ss - if millis are present this will be larger
        maxValue: -1,
        minValue: -1,
        showAllOnStart: false,
        markers: null,
        range: false,
        follow: false,
        alwaysShowDate : false,
        rezoom: null
    },
    
    initialize: function (options) {
        L.Util.setOptions(this, options);
        this._layer = this.options.layer;
    
    },

    getMarkerCount: function(options) {
        //return Object.keys(options.map._layers).length-2;
        var mcount = 0
        options.map.eachLayer((l) => {
            if (l instanceof L.Marker) mcount++;
        })
        return mcount;
    },

    getMarkerCountText: function(count) {
        return "Marker Count: " + count;
    },
    
    extractTimestamp: function(time, options) { //convert time from field to an appropriate date format, 'time' variable should be set when instantiating this object
        //time could be in many different forms, for our simplified purpose we will start with just using 4 digit years
        return time;
    },
    
    setPosition: function (position) { //set the position of the slider control
        var map = this._map;
    
        if (map) {
            map.removeControl(this);
        }
    
        this.options.position = position;
    
        if (map) {
            map.addControl(this);
        }
        this.startSlider(); //call start slider function mentioned at the end of this file
        return this;
    },
    
    onAdd: function (map) { //when this control is added to the map for the first time
        this.options.map = map;
    
        // Create a control sliderContainer with a jquery ui slider
        var sliderContainer = L.DomUtil.create('div', 'slider', this._container);
        $(sliderContainer).append(
            '<div id="leaflet-slider"><div class="ui-slider-handle"></div><div id="slider-timestamp"></div></div>');
        
        //Prevent map panning/zooming while using the slider
        $(sliderContainer).mousedown(function () {
            map.dragging.disable();
        });

        //Release the mouse, reenable panning
        $(document).mouseup(function () {
            map.dragging.enable();
            //Hide the slider timestamp if not range or option alwaysShowDate is set on false
            // if (options.range || !options.alwaysShowDate) {
            //      $('#slider-timestamp').html(''); //resets the timestamp text to nothing
            // }
        });
    
        var options = this.options;
        this.options.markers = [];
        this.options.unique_time_values = []; //We want to know which values are unique, as we only want one slider increment for each date (group markers by date)

        //For our layer of markers
        if (this._layer) {
            var flags = [], unique_values = [], len;
            
            //for each marker: create a key in flags[] for it, that way we can just check if flags has that key already instead of comparing each unique_values entry
            this._layer.eachLayer(function (layer) {
                if( flags[layer.options.time]) return;
                flags[layer.options.time] = true;
                unique_values.push(layer.options.time); //push this unique time into the unique_values array
                ++len;
            });

            //sort unique values - this will in turn keep the features in the right order later on
            unique_values.sort() //may need to be extended later to handle dates

            //cut undefined dates from array (We convert undefined to 0000 in the script that calls this for convenience)
            if (unique_values[0] == "0000") unique_values = unique_values.slice(1)   

            //Create an array for time increments, where each time increment is an array of markers (features)
            var all_features = [];
            for (var i=0;i<unique_values.length;i++){
                all_features[i] = []; //one array for each unique_value
            }

            //Push into feature array - gives us an array where each key is a time that points to an array of markers
            var layers = this._layer.getLayers()
            var zero_values = []
            for(var i=0;i<layers.length;i++){
                var index = unique_values.indexOf(layers[i].options.time) //get index for this time
                if (index != -1) all_features[index].push(layers[i]) //push this layer into the array using it's time as a key 
                else zero_values.push(layers[i]) //push undefined start date values into their own array
            }

            //create min max value labels
            var min = unique_values[0]
            var max = unique_values[unique_values.length-1]
            $(sliderContainer).prepend('<div><span><label>' + min + '</label></span>' + '<span class="float-right"><label>' + max + '</label></span></div>')

            //Create tick box to hide/show items with no date
            var undefinedCheckbox = document.createElement("INPUT")
            undefinedCheckbox.setAttribute("type","checkbox")
            undefinedCheckbox.setAttribute("id","showundefined")

            var undefinedDiv = document.createElement("DIV")
            undefinedDiv.setAttribute("id","undefined-div")

            //pop the checkbox into the div, and the div into the sliderContainer
            $(undefinedDiv).append(undefinedCheckbox,'<label for="showundefined"> Show items that have no dates</label></div>')

            $(sliderContainer).append(undefinedDiv)
           
            //Creating the actual Leaflet object to hold each time increment
            for (var i=0;i<all_features.length;i++){
                options.markers[i] = L.featureGroup(all_features[i]);
            }

            //push the undefined values onto the control options as a unique variable
            options.zero_values = L.featureGroup(zero_values);
            var self = this; //access the correct 'this' from the next function

            //Add a counter for the markers
            var markerCountDiv = document.createElement("DIV")
            markerCountDiv.setAttribute("id","markerCountDiv")

            $(markerCountDiv).html('Count: ' + self.getMarkerCount(options))
            $(sliderContainer).append(markerCountDiv)
            
            //add an event to the checkbox that shows/hides these undefined date markers when clicked
            $(undefinedCheckbox).change(function() {
                if (this.checked) { //show markers
                    map.addLayer(options.zero_values)
                }
                else { //remove markers
                    map.removeLayer(options.zero_values)
                }
                $(markerCountDiv).html(self.getMarkerCountText(self.getMarkerCount(options))) //update the text for the marker count with the current number showing
            })

            //display the first set of markers on load
            map.addLayer(options.markers[0])

            //set the initial marker count text
            $(markerCountDiv).html(self.getMarkerCountText(self.getMarkerCount(options)))

            //Finishing up
            options.maxValue = all_features.length - 1;
            this.options = options;
            this.options.unique_time_values = unique_values
            this.options.minValue = 0


        } else {
            console.log("Error: You have to specify a layer via new SliderControl({layer: your_layer});");
        }
        return sliderContainer;

    },
    
    onRemove: function (map) {
        //Delete all markers which where added via the slider and remove the slider div
        for (i = this.options.minValue; i < this.options.maxValue; i++) {
            map.removeLayer(this.options.markers[i]);
        }
        $('#leaflet-slider').remove();
    },
    
    startSlider: function () {
        _options = this.options; //a private copy of options for use here
        _extractTimestamp = this.extractTimestamp
        var index_start = _options.minValue;
        if(_options.showAllOnStart){
            index_start = _options.maxValue;
            if(_options.range) _options.values = [_options.minValue,_options.maxValue];
            else _options.value = _options.maxValue;
        }

        var self = this; //var for calling back to functions in this file from sub-functions (where this no longer refers to this file)
        
        //Display the date for the first set as well
        $('#slider-timestamp').html(_extractTimestamp(_options.unique_time_values[0], _options));

        
        $("#leaflet-slider").slider({ //creating the slider instance for the first time
            range: _options.range,
            value: _options.minValue,
            values: _options.values,
            min: _options.minValue,
            max: _options.maxValue,
            step: 1,
            slide: function (e, ui) { //event for when the slider is moved 
                var map = _options.map;
                var fg = L.featureGroup();
                //updating the timestamp
                if(!!_options.markers[ui.value]) { //if there are markers for this slider value?
                    //console.log('inside');
                    // If there is no time property, this line has to be removed (or exchanged with a different property)
                    if(_options.markers[ui.value].feature !== undefined) { //this seems to be for loading marks by json?
                        if(_options.markers[ui.value].feature.properties[_options.timeAttribute]){
                            if(_options.markers[ui.value]) $('#slider-timestamp').html(
                                _extractTimestamp(_options.unique_values[ui.value].feature.properties[_options.timeAttribute], _options));
                        }else {
                            console.error("Time property "+ _options.timeAttribute +" not found in data");
                        }
                    }else {
                        // set by leaflet Vector Layers
                        if(_options.unique_time_values[ui.value]){ //if this slider tick refers to a unique time value (it always should)
                            if(_options.markers[ui.value]) {
                                if (_options.range) {
                                    $('#slider-timestamp').html( //if there is a marker for this slider value (there always should be)
                                        //get left slider val + " - " + get right slider val
                                        _extractTimestamp(_options.unique_time_values[ui.value], _options) + " - " + ""
                                    ); //set the slider timestamp to be the time for this increment
                                }
                                else $('#slider-timestamp').html( //if there is a marker for this slider value (there always should be)
                                    _extractTimestamp(_options.unique_time_values[ui.value], _options)); //set the slider timestamp to be the time for this increment
                            }
                        }else {
                            console.error("Time property "+ _options.timeAttribute +" not found in data");
                        }
                    }
    
                    var i;
                    // clear markers
                    for (i = _options.minValue+1; i <= _options.maxValue; i++) {
                        if(_options.markers[i]) map.removeLayer(_options.markers[i]); 
                    }
                    if(_options.range){
                        // jquery ui using range
                        for (i = ui.values[0]; i <= ui.values[1]; i++){
                           if(_options.markers[i]) {
                               map.addLayer(_options.markers[i]);
                               fg.addLayer(_options.markers[i]);
                           }
                        }
                    }else if(_options.follow){
                        for (i = ui.value - _options.follow + 1; i <= ui.value ; i++) {
                            if(_options.markers[i]) {
                                map.addLayer(_options.markers[i]);
                                fg.addLayer(_options.markers[i]);
                            }
                        }
                    }else{
                        for (i = _options.minValue; i <= ui.value ; i++) {
                            if(_options.markers[i]) {
                                map.addLayer(_options.markers[i]);
                                fg.addLayer(_options.markers[i]);
                            }
                        }
                    }
                };
                if(_options.rezoom) {
                    map.fitBounds(fg.getBounds(), {
                        maxZoom: _options.rezoom
                    });
                }
                $('#markerCountDiv').html(self.getMarkerCountText(self.getMarkerCount(_options)))
            }
        });
        if (!_options.range && _options.alwaysShowDate) {
            $('#slider-timestamp').html(_extractTimeStamp(_options.markers[index_start].feature.properties[_options.timeAttribute], _options));
        }
        for (i = _options.minValue; i < index_start; i++) {
            _options.map.addLayer(_options.markers[i]);
        }
    }

});
    
L.control.sliderControl = function (options) {
    return new L.Control.SliderControl(options);
};


